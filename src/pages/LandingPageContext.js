import React, { useState, createContext } from "react";

export const LandingPageContext = createContext();

export const LandingPageProvider = props => {
    const [covid, setCovid] =  useState([])
    const [covidBar, setCovidBar] =  useState([])
    // const [datenow, setDatenow] = useState(new Date())
    const [sidebar, setSidebar] = useState(false); 
    const [detailCovid, setDetail] = useState({cases:0, deaths:0, recovered:0, last_update:new Date()})

    return (
        <LandingPageContext.Provider value={[covid, setCovid, covidBar, setCovidBar, 
                                            detailCovid, setDetail,sidebar, setSidebar]}>
            {props.children}
        </LandingPageContext.Provider>
    );
};