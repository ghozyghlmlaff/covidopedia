import React from 'react'
import Navigation from './components/navigation';
import Header from './components/headers';
import Charts from './components/chart';
import Credits from './components/credits';
import Advices from './components/advices';
import { LandingPageProvider } from './pages/LandingPageContext';
import Sidebar from './components/Sidebar';
import "./style/scss/style.scss"
import "./style/scss/navbar.scss"
import "./style/scss/headers.scss"
import "./style/scss/chart.scss"
import "./style/scss/credit.scss"
import "./style/scss/contact.scss"
import "./style/scss/sidebar.scss"
import {
  ApolloClient,
  InMemoryCache,
  ApolloProvider,
} from "@apollo/client";

const client = new ApolloClient({
  uri: 'https://covid19-graphql.now.sh/',
  cache: new InMemoryCache()
});

const App =()=> {
    return (
      <div>
        <ApolloProvider client={client}>
          <LandingPageProvider>
            <Navigation />
            <Sidebar/>
            <div className="content" id="main">
              <Header />
              <Charts />
              <Credits />
              <Advices />
            </div>
          </LandingPageProvider>
        </ApolloProvider>
      </div>
    )
}

export default App;
