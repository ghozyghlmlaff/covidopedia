import React, { useEffect, useContext, useState } from "react";
import { LandingPageContext } from "../pages/LandingPageContext";
import axios from "axios";
import { Line, Bar } from 'react-chartjs-2';

const Charts =()=> {
    var days = ['Ahad', 'Senin', 'Selasa', 'Rabu', 'Kamis', 'Jum\'at', 'Sabtu'];
    var month = ['Januari', 'Februari', 'Maret', 'April', 'Mei', 'Juni', 'Juli', 'Agustus', 'September', 'Oktober', 'November', 'Desember'];
    const [covid, setCovid, covidBar, setCovidBar, detailCovid, setDetail] = useContext(LandingPageContext)
    const [datenow, setDatenow] = useState(new Date())
    const [isLoading, setLoading] = useState({line:true, bar:true});

    var dayName = days[datenow.getDay()]
    var monthName = month[datenow.getMonth()]

    var options_line = {
        maintainAspectRatio : false,
        type:'line',
        responsive:true,
        legend: {
            position: "top",
            align: "left"
        },
        tooltips: {
            enabled: true,
            fill:false,
            callbacks: {
                label: function(tooltipItems, data) {
                    var label =  data.datasets[tooltipItems.datasetIndex].label || '';
                    var currentValue =
                        data.datasets[tooltipItems.datasetIndex].data[tooltipItems.index]
                    if (label) {
                        label += ': ';
                    }
                    label += currentValue;
                    return label; 
                }
            }
        }
    }
    var options_bar = {
        maintainAspectRatio : false,
        type:'bar',
        responsive:true,
        legend: { display: false },
        title: {
            display: true,
            text: 'Histogram 10 Negara dengan Covid-19 Terbanyak'
        }
    }


    useEffect( () => {
        if (covid.length === 0){
            axios({
            url: 'https://covid19-api.org/api/status/id',
            headers: { 'content-type': 'application/json' },
            method: 'get',          
            }).then((result) => {
                var covids = result.data;
                setDetail({cases:covids.cases, deaths:covids.deaths, recovered:covids.recovered})
                setDatenow(new Date(covids.last_update))
            });
            axios({
            url: 'https://covid19-api.org/api/timeline/id',
            headers: { 'content-type': 'application/json' },
            method: 'get',          
            }).then((result) => {
                var covids = result.data;
                var labels=[];

                var konfirmasi=[];
                var sembuh=[];
                var meninggal=[];

                for (var i=0;i<30;i = i+1){
                var date = new Date(covids[i].last_update)
                labels.push(date.toLocaleDateString('en-GB'))
                konfirmasi.push(covids[i].cases);  
                sembuh.push(covids[i].recovered);
                meninggal.push(covids[i].deaths);  
                }
                var dd = {
                labels: labels.reverse(),
                datasets:[
                    {
                        label: 'Konfirmasi',
                        backgroundColor: 'orange',
                        fill:false,
                        borderColor: "orange",
                        borderWidth: 1,
                        data:  konfirmasi.reverse()
                    },
                    {
                        label: 'Sembuh',
                        backgroundColor: 'green',
                        fill:false,
                        borderColor: "green",
                        borderWidth: 1,
                        data: sembuh.reverse()
                    },

                    {
                        label: 'Meninggal',
                        backgroundColor: 'red',
                        fill:false,
                        borderColor: 'red',
                        borderWidth: 1,
                        data: meninggal.reverse(),
                    }
                ]
            }
            setCovid(dd);
            setLoading({line:false});
            });
        }
        if (covidBar.length === 0){
            axios({
            url: 'https://corona.lmao.ninja/v3/covid-19/countries?sort=cases',
            headers: { 'content-type': 'application/json' },
            method: 'get',
            }).then((result) => {
                var covidbar = result.data;
                var labels=[];
                var datas=[];

                for (var i=0;i<10;i = i+1){
                labels.push(covidbar[i].country)
                datas.push(covidbar[i].cases)
                }
                var dd = {
                labels: labels,
                datasets: [
                    {
                        label: "Jumlah",
                        backgroundColor: ["#3e95cd", "#8e5ea2","#3cba9f","#e8c3b9","#c45850", "#F4B93D", "#B4DDBF", "#4A4A4A", "#EABAD8"],
                        data: datas
                    }
                ]
                }
            setCovidBar(dd);
            setLoading({bar:false});
            });
        }
    })

    return (
        <div id="charts" className="text-center">
            <div className="container" style={{maxWidth:"1200px"}}>
            <div className="section-title">
                <h2 className="text-center">Charts</h2>
            </div>
            <div className="panel-body">
                <div className="title-covid">
                <h2 className="head-title">COVID-19 Situation Report for Indonesia</h2>
                <h3 className="head-title">Terhitung sejak : {dayName}, {datenow.getDate()} {monthName} {datenow.getFullYear()}</h3>
                </div>
                <br/>
                <div className="logo-chart">
                <div className="detail-logo konfirmasi">
                    <div className="dot-logo">
                    <i className="fa fa-ambulance fa-2x" style={{color:"orange", marginTop:"15px"}} aria-hidden="true"></i>
                    </div>
                    <h4>{detailCovid.cases.toLocaleString('en-US')}</h4>
                    <p className="detail">Konfirmasi</p>
                </div>
                <div className="detail-logo meninggal">
                    <div className="dot-logo">
                    <i className="fa fa-bed fa-2x" style={{color:"red", marginTop:"15px"}} aria-hidden="true"></i>
                    </div>
                    <h4>{detailCovid.deaths.toLocaleString('en-US')}</h4>
                    <p className="detail">Meninggal</p>
                </div>
                <div className="detail-logo sembuh">
                    <div className="dot-logo">
                    <i className="fa fa-plus-square  fa-2x" style={{color:"green", marginTop:"15px"}} aria-hidden="true"></i>
                    </div>
                    <h4>{detailCovid.recovered.toLocaleString('en-US')}</h4>
                    <p className="detail">Sembuh</p>
                </div>
                </div>
                <br/>
                <div className="chartline">
                <h3>Grafik Perkembangan 30 Hari Terakhir</h3>
                {isLoading.line ? 'Loading ...' :
                <Line options={options_line} data={covid} />
                }
                </div>
                <div className="chartline">
                <h3>10 Negara dengan Kasus Covid-19 Terbanyak</h3>
                {isLoading.bar ? 'Loading ...' :
                <Bar options={options_bar} data={covidBar}/>
                }
                </div>
            </div>          
            </div>
        </div>
    );
}

export default Charts;
