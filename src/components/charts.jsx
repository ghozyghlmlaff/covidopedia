// import React, { useEffect, useContext } from "react";
// import { LandingPageContext } from "../pages/LandingPageContext";
// import axios from "axios";
// import { Line, Bar } from 'react-chartjs-2';
// import {
//   useQuery,
//   gql,
// } from "@apollo/client";

// const Charts =()=> {
//     var days = ['Ahad', 'Senin', 'Selasa', 'Rabu', 'Kamis', 'Jum\'at', 'Sabtu'];
//     var month = ['Januari', 'Februari', 'Maret', 'April', 'Mei', 'Juni', 'Juli', 'Agustus', 'September', 'Oktober', 'November', 'Desember'];
//     const [, , covidBar, setCovidBar, datenow,,, ] = useContext(LandingPageContext)
//     var dayName = days[datenow.getDay()]
//     var monthName = month[datenow.getMonth()]

//     var options_line = {
//       maintainAspectRatio : false,
//       type:'line',
//       responsive:true,
//       legend: {
//           position: "top",
//           align: "left"
//       },
//       tooltips: {
//           enabled: true,
//           fill:false,
//           callbacks: {
//               label: function(tooltipItems, data) {
//                   var label =  data.datasets[tooltipItems.datasetIndex].label || '';
//                   var currentValue =
//                       data.datasets[tooltipItems.datasetIndex].data[tooltipItems.index]
//                   if (label) {
//                       label += ': ';
//                   }
//                   label += currentValue;
//                   return label; 
//               }
//           }
//       }
//     }
//     var options_bar = {
//       maintainAspectRatio : false,
//       type:'bar',
//       responsive:true,
//       legend: { display: false },
//       title: {
//           display: true,
//           text: 'Histogram 10 Negara dengan Covid-19 Terbanyak'
//       }
//     }

//     var now = new Date()
//     var date = new Date(now.setDate(now.getDate()-30))

//     const GET_RESULTS = gql`
//     query { 
//         results (countries: ["Indonesia"], date: { gt: "${date.toLocaleDateString()}" }) {
//         country {
//             name
//         }
//         date
//         confirmed
//         deaths
//         recovered
//         growthRate
//         }
//     }
//     `;
//     const GetCovid =()=> {
//       const { data, loading, error } = useQuery(GET_RESULTS)
//       if (loading) return 'Loading...'
//       if (error) return `Error! ${error.message}`
//       var covids = data.results;
//       var labels=[];

//       var konfirmasi=[];
//       var sembuh=[];
//       var meninggal=[];

//       for (var i=0;i<covids.length;i = i+1){
//         var date = new Date(covids[i].date)
//         labels.push(date.toLocaleDateString('en-GB'))
//         konfirmasi.push(covids[i].confirmed);  
//         sembuh.push(covids[i].recovered);
//         meninggal.push(covids[i].deaths);  
//       }
//       var dd = {
//         labels: labels,
//         datasets:[
//             {
//                 label: 'Konfirmasi',
//                 backgroundColor: 'orange',
//                 fill:false,
//                 borderColor: "orange",
//                 borderWidth: 1,
//                 data:  konfirmasi
//             },
//             {
//                 label: 'Sembuh',
//                 backgroundColor: 'green',
//                 fill:false,
//                 borderColor: "green",
//                 borderWidth: 1,
//                 data: sembuh 
//             },

//             {
//                 label: 'Meninggal',
//                 backgroundColor: 'red',
//                 fill:false,
//                 borderColor: 'red',
//                 borderWidth: 1,
//                 data: meninggal,
//             }
//         ]
//       }
//       return (
//         <>
//         <div className="logo-chart">
//           <div className="detail-logo konfirmasi">
//             <div className="dot-logo">
//               <i className="fa fa-ambulance fa-2x" style={{color:"orange", marginTop:"15px"}} aria-hidden="true"></i>
//             </div>
//             <h4>{konfirmasi[konfirmasi.length-1].toLocaleString('en-US')}</h4>
//             <p className="detail">Konfirmasi</p>
//           </div>
//           <div className="detail-logo meninggal">
//             <div className="dot-logo">
//               <i className="fa fa-bed fa-2x" style={{color:"red", marginTop:"15px"}} aria-hidden="true"></i>
//             </div>
//             <h4>{meninggal[meninggal.length-1].toLocaleString('en-US')}</h4>
//             <p className="detail">Meninggal</p>
//           </div>
//           <div className="detail-logo sembuh">
//             <div className="dot-logo">
//               <i className="fa fa-plus-square  fa-2x" style={{color:"green", marginTop:"15px"}} aria-hidden="true"></i>
//             </div>
//             <h4>{sembuh[sembuh.length-1].toLocaleString('en-US')}</h4>
//             <p className="detail">Sembuh</p>
//           </div>
//         </div>
//         <br/>
//         <div className="chartline">
//           <h3>Grafik Perkembangan {konfirmasi.length} Hari Terakhir</h3>
//           <Line options={options_line} data={dd} />
//         </div>
//         </>
//       );
//     }
    
//     // const { data, loading, error } = useQuery(GET_RESULTS)
    
//   // const query = `{ 
//   //   results (countries: ["Indonesia"], date: { gt: "${date.toLocaleDateString()}" }) {
//   //     country {
//   //       name
//   //     }
//   //     date
//   //     confirmed
//   //     deaths
//   //     recovered
//   //     growthRate
//   //   }
//   // }`;


//     useEffect( () => {
//       // if (loading === false && data){
//       //   var covids = data.results;
//       //   var labels=[];

//       //   var konfirmasi=[];
//       //   var sembuh=[];
//       //   var meninggal=[];

//       //   for (var i=0;i<covids.length;i = i+1){
//       //     var date = new Date(covids[i].date)
//       //     labels.push(date.toLocaleDateString('en-GB'))
//       //     konfirmasi.push(covids[i].confirmed);  
//       //     sembuh.push(covids[i].recovered);
//       //     meninggal.push(covids[i].deaths);  
//       //     if (i===covids.length-1){
//       //       setDetail({konfirmasi:covids[i].confirmed, sembuh:covids[i].recovered, meninggal:covids[i].deaths})
//       //     }
//       //   }
//       //   var dd = {
//       //     labels: labels,
//       //     datasets:[
//       //         {
//       //             label: 'Konfirmasi',
//       //             backgroundColor: 'orange',
//       //             fill:false,
//       //             borderColor: "orange",
//       //             borderWidth: 1,
//       //             data:  konfirmasi
//       //         },
//       //         {
//       //             label: 'Sembuh',
//       //             backgroundColor: 'green',
//       //             fill:false,
//       //             borderColor: "green",
//       //             borderWidth: 1,
//       //             data: sembuh 
//       //         },

//       //         {
//       //             label: 'Meninggal',
//       //             backgroundColor: 'red',
//       //             fill:false,
//       //             borderColor: 'red',
//       //             borderWidth: 1,
//       //             data: meninggal,
//       //         }
//       //     ]
//       //   } 
//       //   setCovid(dd)   
//       // }
//       // if (covid.length === 0){
//         // axios({
//         //   url: 'https://covid19-graphql.now.sh/',
//         //   headers: { 'content-type': 'application/json' },
//         //   method: 'post',
//         //   data: {
//         //     query,
//         //   },
          
//         // }).then((result) => {
//         //     var covids = result.data.data.results;
//         //     var labels=[];

//         //     var konfirmasi=[];
//         //     var sembuh=[];
//         //     var meninggal=[];

//         //     for (var i=0;i<covids.length;i = i+1){
//         //       var date = new Date(covids[i].date)
//         //       labels.push(date.toLocaleDateString('en-GB'))
//         //       konfirmasi.push(covids[i].confirmed);  
//         //       sembuh.push(covids[i].recovered);
//         //       meninggal.push(covids[i].deaths);  
//         //       if (i===covids.length-1){
//         //         setDetail({konfirmasi:covids[i].confirmed, sembuh:covids[i].recovered, meninggal:covids[i].deaths})
//         //       }
//         //     }
//         //     var dd = {
//         //       labels: labels,
//         //       datasets:[
//         //           {
//         //               label: 'Konfirmasi',
//         //               backgroundColor: 'orange',
//         //               fill:false,
//         //               borderColor: "orange",
//         //               borderWidth: 1,
//         //               data:  konfirmasi
//         //           },
//         //           {
//         //               label: 'Sembuh',
//         //               backgroundColor: 'green',
//         //               fill:false,
//         //               borderColor: "green",
//         //               borderWidth: 1,
//         //               data: sembuh 
//         //           },

//         //           {
//         //               label: 'Meninggal',
//         //               backgroundColor: 'red',
//         //               fill:false,
//         //               borderColor: 'red',
//         //               borderWidth: 1,
//         //               data: meninggal,
//         //           }
//         //       ]
//         //   }
//         //   setCovid(dd);
//         // });
//       // }
//       if (covidBar.length === 0){
//         axios({
//           url: 'https://corona.lmao.ninja/v3/covid-19/countries?sort=cases',
//           headers: { 'content-type': 'application/json' },
//           method: 'get',
//         }).then((result) => {
//             var covidbar = result.data;
//             var labels=[];
//             var datas=[];

//             for (var i=0;i<10;i = i+1){
//               labels.push(covidbar[i].country)
//               datas.push(covidbar[i].cases)
//             }
//             var dd = {
//               labels: labels,
//               datasets: [
//                   {
//                       label: "Jumlah",
//                       backgroundColor: ["#3e95cd", "#8e5ea2","#3cba9f","#e8c3b9","#c45850", "#F4B93D", "#B4DDBF", "#4A4A4A", "#EABAD8"],
//                       data: datas
//                   }
//               ]
//             }
//           setCovidBar(dd);
//         });
//       }
//     })
  
//     return (
//       <div id="charts" className="text-center">
//         <div className="container">
//           <div className="section-title">
//             <h2 className="text-center">Charts</h2>
//           </div>
//           <div className="panel-body">
//             <div className="title-covid">
//               <h2 className="head-title">COVID-19 Situation Report for Indonesia</h2>
//               <h3 className="head-title">{dayName}, {datenow.getDate()} {monthName} {datenow.getFullYear()}</h3>
//             </div>
//             <br/>
//             {/* <div className="logo-chart">
//               <div className="detail-logo konfirmasi">
//                 <div className="dot-logo">
//                   <i className="fa fa-ambulance fa-2x" style={{color:"orange", marginTop:"15px"}} aria-hidden="true"></i>
//                 </div>
//                 <h4>{detailCovid.konfirmasi.toLocaleString('en-US')}</h4>
//                 <p className="detail">Konfirmasi</p>
//               </div>
//               <div className="detail-logo meninggal">
//                 <div className="dot-logo">
//                   <i className="fa fa-bed fa-2x" style={{color:"red", marginTop:"15px"}} aria-hidden="true"></i>
//                 </div>
//                 <h4>{detailCovid.meninggal.toLocaleString('en-US')}</h4>
//                 <p className="detail">Meninggal</p>
//               </div>
//               <div className="detail-logo sembuh">
//                 <div className="dot-logo">
//                   <i className="fa fa-plus-square  fa-2x" style={{color:"green", marginTop:"15px"}} aria-hidden="true"></i>
//                 </div>
//                 <h4>{detailCovid.sembuh.toLocaleString('en-US')}</h4>
//                 <p className="detail">Sembuh</p>
//               </div>
//             </div>
//             <br/> */}
//             <GetCovid/>
//             {/* <div className="chartline">
//               <h3>Grafik Perkembangan 30 Hari Terakhir</h3>
//               <Line options={options_line} data={covid} />
//             </div> */}
//             <div className="chartline">
//               <h3>10 Negara dengan Kasus Covid-19 Terbanyak</h3>
//               <Bar options={options_bar} data={covidBar}/>
//             </div>
//           </div>          
//         </div>
//       </div>
//     );
// }

// export default Charts;
